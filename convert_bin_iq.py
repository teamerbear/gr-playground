#! /usr/bin/env python3.7

import os
import struct
import sys


def get_iq_from_file(file_path):
  i_samples = []
  q_samples = []
  with open(file_path, "rb") as bin_file:
    # Assuming format is 32-bit float I/Q interleaved.
    next_flt = bin_file.read(4)
    q = False
    while next_flt:
      flt_sample = struct.unpack('f', next_flt)
      if q:
        q_samples.append(flt_sample)
        q = False
      else:
        i_samples.append(flt_sample)
        q = True
      next_flt = bin_file.read(4)

  return (i_samples, q_samples)


def write_iq_to_file(samples, file_path):
  i_samples = samples[0]
  q_samples = samples[1]

  with open(file_path, 'w') as outfile:
    for i in range(len(i_samples)):
      outfile.write("{} {}\n".format(i_samples[i], q_samples[i]))


if __name__ == "__main__":
  if not len(sys.argv) >= 3:
    raise ValueError(
        "USAGE: python3 convert_bin_iq.py <input_file> <output_file>")
  
  in_file = sys.argv[1]
  out_file = sys.argv[2]

  samples = get_iq_from_file(in_file) 
  write_iq_to_file(samples, out_file)
