#! /usr/bin/env python3.7

import os
import struct
import sys

def get_lines_from_file(file_path):
  lines = []
  with open(file_path, 'r') as infile:
    lines = infile.readlines()
  
  return lines


if __name__ == "__main__":
  if not len(sys.argv) >= 3:
    raise ValueError("USAGE: python3 compare_files.py <file_1> <file_2>")
  
  lines_1 = get_lines_from_file(sys.argv[1])
  lines_2 = get_lines_from_file(sys.argv[2])

  if not (len(lines_1) == len(lines_2)):
    raise ValueError("The two files are not the same length!")
  
  equal = True
  for i in range(len(lines_1)):
    if not (lines_1[i] == lines_2[i]):
      print("Files are not equal! (line {})".format(i + 1))
      equal = False
      break
  
  if equal:
    print("Files are equal!")