#! /usr/bin/env python3.7

import struct
import sys


def get_bytes_from_file(file_path):
  byte_arr = []
  with open(file_path, "rb") as bin_file:
    next_byte = bin_file.read(1)
    while next_byte:
      byte_arr.append(struct.unpack('B', next_byte))
      next_byte = bin_file.read(1)
  
  return byte_arr


def write_bytes_to_file(byte_arr, file_path):
  with open(file_path, 'w') as outfile:
    for i in range(len(byte_arr)):
      outfile.write("{}\n".format(byte_arr[i]))


if __name__ == "__main__":
  if not len(sys.argv) >= 3:
    raise ValueError(
        "USAGE: python3 convert_bin_bytes.py <input_file> <output_file>")
  
  in_file = sys.argv[1]
  out_file = sys.argv[2]

  byte_arr = get_bytes_from_file(in_file)
  write_bytes_to_file(byte_arr, out_file)
