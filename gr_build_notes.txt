--->  Some of the ports you installed have notes:
  OpenBLAS has the following notes:
    This version is built based on a base architecture for convenience, 
    which may not be optimized for your system. To build a version 
    customized for your machine, use the +native variant
  coreutils has the following notes:
    The tools provided by GNU coreutils are prefixed with the character 'g' by default to distinguish them from the BSD commands.
    For example, cp becomes gcp and ls becomes gls.
    
    If you want to use the GNU tools by default, add this directory to the front of your PATH environment variable:
        /opt/local/libexec/gnubin/
  dbus has the following notes:
    Startup items (named 'dbus-system, dbus-session') have been generated that will aid in starting dbus with launchd. They are disabled by
    default. Execute the following command to start them, and to cause them to launch at startup:
    
        sudo port load dbus
  grep has the following notes:
    This port previously installed itself without a g* prefix, thus overshadowing system binaries such as grep, fgrep, and egrep. The port is now
    changed so that it does install with a g* prefix, like other GNU ports. This means that you'll now find GNU grep at /opt/local/bin/ggrep. If
    you dislike typing ggrep, you can create a shell alias or you can add /opt/local/libexec/gnubin to your PATH, wherein non-g* prefixed symlinks
    are installed. In other words, /opt/local/libexec/gnubin contains GNU binaries without any prefix to the file names, so you can type grep and
    get GNU grep just as before.
  libomp has the following notes:
    To use this OpenMP library:
     * For clang-3.8+, or clang-3.7 with +openmp variant:
        add "-fopenmp" during compilation / linking.
     * For clang-3.7 without +openmp variant, use:
        "-I/opt/local/include/libomp -L/opt/local/lib/libomp -fopenmp"
  libpsl has the following notes:
    libpsl API documentation is provided by the port 'libpsl-docs'.
  lzma has the following notes:
    The LZMA SDK program is installed as "lzma_alone", to avoid conflict with LZMA Utils
  py37-cython has the following notes:
    To make the Python 3.7 version of Cython the one that is run when you execute the commands without a version suffix, e.g. 'cython', run:
    
    port select --set cython cython37
  py37-matplotlib has the following notes:
    The default backend is the interactive Mac OS X backend. Different backends can be specified using the ~/.matplotlib/matplotlibrc file. More
    details regarding backends can be found in the matplotlib FAQ:
    
            https://matplotlib.org/tutorials/introductory/usage.html#backends
  py37-sip has the following notes:
    py37-sip is available under a PSF license with one addition: 
    
        4. Licensee may not use SIP to generate Python bindings for any C or
           C++ library for which bindings are already provided by Riverbank. 
    
    GPL-2 or GPL-3 licenses are also available. For details see
    /opt/local/share/doc/py37-sip/LICENSE [-GPL2 -GPL3]
  py38-cython has the following notes:
    To make the Python 3.8 version of Cython the one that is run when you execute the commands without a version suffix, e.g. 'cython', run:
    
    port select --set cython cython38
  py38-docutils has the following notes:
    To make the Python 3.8 version of docutils the one that is run when you execute the commands without a version suffix, e.g. 'rst2man', run:
    
    port select --set docutils py38-docutils
  python37 has the following notes:
    To make this the default Python or Python 3 (i.e., the version run by the 'python' or 'python3' commands), run one or both of:
    
        sudo port select --set python python37
        sudo port select --set python3 python37
  python38 has the following notes:
    To make this the default Python or Python 3 (i.e., the version run by the 'python' or 'python3' commands), run one or both of:
    
        sudo port select --set python python38
        sudo port select --set python3 python38
  python39 has the following notes:
    To make this the default Python or Python 3 (i.e., the version run by the 'python' or 'python3' commands), run one or both of:
    
        sudo port select --set python python39
        sudo port select --set python3 python39
  tcl has the following notes:
    The Sqlite3 Tcl package is now being provided by the sqlite3-tcl port:
    sudo port install sqlite3-tcl

