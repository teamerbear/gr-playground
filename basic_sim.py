#! /usr/bin/env python3.7

from gnuradio import blocks, digital, gr
import numpy as np

class BasicSimTop(gr.top_block):

  def __init__(self, mod_level=2, data_in=None):
    """ """
    gr.top_block.__init__(self, name="basic_sim_top")

    # Generate source bit stream if needed.
    self._src_bits = data_in
    if self._src_bits is None:
      self._src_bits = np.random.randint(2, size=(1e6,), dtype=np.int8)

    # Instantiate the blocks that are part of the simulation.
    self._bit_src = blocks.vector_source_b(self._src_bits)
    self._packer = blocks.unpacked_to_packed_bb(
        bits_per_chunk=1, endianness=gr.GR_MSB_FIRST)
    self._mod = digital.psk_mod(
        constellation_points=mod_level, samples_per_symbol=4)
    
    # TODO: Need to add some sort of timing recovery here? to sync up to the
    #       symbols.
    self._demod = digital.psk_demod(
        constellation_points=mod_level, samples_per_symbol=4)
    self._unpacker = blocks.packed_to_unpacked_bb(
        bits_per_chunk=1, endianness=gr.GR_MSB_FIRST)
    self._bit_sink_b = blocks.vector_sink_b()
    self._bit_sink_c = blocks.vector_sink_c()

    # Connect up the blocks.
    self._path = 4

    if self._path == 0:
      self.connect((self._bit_src, 0), (self._bit_sink_b, 0))

    elif self._path == 1:
      self.connect((self._bit_src, 0), (self._packer, 0))
      self.connect((self._packer, 0), (self._bit_sink_b, 0))

    elif self._path == 2:
      self.connect((self._bit_src, 0), (self._packer, 0))
      self.connect((self._packer, 0), (self._mod, 0))
      self.connect((self._mod, 0), (self._bit_sink_c, 0))

    elif self._path == 3:
      self.connect((self._bit_src, 0), (self._packer, 0))
      self.connect((self._packer, 0), (self._mod, 0))
      self.connect((self._mod, 0), (self._demod, 0))
      self.connect((self._demod, 0), (self._bit_sink_b, 0))

    elif self._path == 4:
      self.connect((self._bit_src, 0), (self._packer, 0))
      self.connect((self._packer, 0), (self._mod, 0))
      self.connect((self._mod, 0), (self._demod, 0))
      self.connect((self._demod, 0), (self._unpacker, 0))
      self.connect((self._unpacker, 0), (self._bit_sink_b, 0))

  def get_output_data(self):
    out_data = None
    if self._path == 2:
      out_data = self._bit_sink_c.data()
    else:
      out_data = self._bit_sink_b.data()
    
    return out_data


def main():
  src_data = np.random.randint(2, size=(64,), dtype=np.int8)
  top_block = BasicSimTop(mod_level=2, data_in=src_data)
  print("=====================================================================")
  top_block.run()
  out_data = top_block.get_output_data()
  print("\nInput data length: {}\nOutput data length: {}\n\nInput data: {}\n\n"
        "Output data: {}\n".format(
          len(src_data), len(out_data), src_data, out_data))
  print("=====================================================================")


if __name__ == "__main__":
  main()